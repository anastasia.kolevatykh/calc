package org.kolevatykh;

import javax.xml.ws.Endpoint;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Endpoint.publish("http://0.0.0.0:8080/CalculatorEndpoint?wsdl", new CalculatorEndpoint());
        System.out.println( "Hello World!" );

        //SOAP UI
    }
}
