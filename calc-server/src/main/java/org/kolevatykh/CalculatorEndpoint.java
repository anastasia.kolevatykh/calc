package org.kolevatykh;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
//(endpointInterface = "org.kolevatykh.Calculator")
public class CalculatorEndpoint {

    static String url = "http://0.0.0.0:8080/CalculatorEndpoint?wsdl";

    @WebMethod
    public int sum(
        @WebParam(name = "a") int a,
        @WebParam(name = "b") int b
    ) {
        return a + b;
    }
}
