// <auto-generated>
// 
//
// Generated by <a href="http://enunciate.webcohesion.com">Enunciate</a>.
// </auto-generated>
using System;

namespace Org.Kolevatykh {


  /// <remarks>
  ///  (no documentation provided)
  /// </remarks>
  /// <summary>
  ///  (no documentation provided)
  /// </summary>
  [System.Web.Services.WebServiceBindingAttribute(Name="CalculatorEndpoint",Namespace="http://kolevatykh.org/")]
  public partial class CalculatorEndpoint : System.Web.Services.Protocols.SoapHttpClientProtocol {


    /// <summary>
    ///  Construct a CalculatorEndpoint that points to the endpoint at a specified host and port.
    /// </summary>
    /// <param name="host">The host of the endpoint.</param>
    /// <param name="port">The port of the endpoint.</param>
    public CalculatorEndpoint(string host, int port) {
      UriBuilder builder = new UriBuilder("http://localhost:8080/CalculatorEndpointService");
      builder.Host = host;
      builder.Port = port;
      this.Url = builder.Uri.ToString();
    }

    /// <summary>
    ///  Construct a CalculatorEndpoint that points to the specified endpoint.
    /// </summary>
    /// <param name="url">The URL of the endpoint.</param>
    public CalculatorEndpoint(string url) {
      this.Url = url;
    }

    /// <summary>
    ///  (no summary provided)
    /// </summary>
    /// <param name="a">(no parameter documentation provided)</param>
    /// <param name="b">(no parameter documentation provided)</param>
    /// <returns></returns>
    [System.Web.Services.Protocols.SoapDocumentMethodAttribute(Action="",Use=System.Web.Services.Description.SoapBindingUse.Literal,ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped,RequestElementName="sum",RequestNamespace="http://kolevatykh.org/",ResponseElementName="sumResponse",ResponseNamespace="http://kolevatykh.org/")]
    [return:System.Xml.Serialization.XmlElementAttribute(ElementName="return",Namespace="")]
    public int Sum([System.Xml.Serialization.XmlElementAttribute(ElementName="a",Namespace="")] int a, [System.Xml.Serialization.XmlElementAttribute(ElementName="b",Namespace="")] int b) {
      object[] results = this.Invoke("Sum", new object[] { a, b });
      return ((int)(results[0]));
    }

  }
}
