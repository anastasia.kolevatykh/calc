/**
 * 
 *
 * Generated by <a href="http://enunciate.webcohesion.com">Enunciate</a>.
 */
package org.kolevatykh.jaxws;

import javax.xml.namespace.QName;

/**
 * Request bean for the sum operation.
 */
@javax.xml.bind.annotation.XmlRootElement (
  name = "sum",
  namespace = "http://kolevatykh.org/"
)
@javax.xml.bind.annotation.XmlType (
  name = "sum",
  namespace = "http://kolevatykh.org/",
  propOrder = { "a", "b" }
)
@javax.xml.bind.annotation.XmlAccessorType ( javax.xml.bind.annotation.XmlAccessType.FIELD )
public class Sum {

  @javax.xml.bind.annotation.XmlElement (
    name = "a"
  )
  protected int a;
  @javax.xml.bind.annotation.XmlElement (
    name = "b"
  )
  protected int b;

  /**
   * 
   */
  public int getA() {
    return this.a;
  }

  /**
   * 
   */
  public void setA(int a) {
    this.a = a;
  }

  /**
   * 
   */
  public int getB() {
    return this.b;
  }

  /**
   * 
   */
  public void setB(int b) {
    this.b = b;
  }
}
